# Quality Engineer UI Technical Test

## Update (2-Oct-2022): I provided a solution in Java. Of course, many things can be added, such as:
1. Logging
2. CI (I will try to add while the submission is on its way)
3. Unit tests of the framework itself
4. More tests :)
5. Implicit/Explicit wait
6. More wrapping of methods and more precise exception handling in them
7. Better post-mortem (screenshots, test the web exception message - if it's nice and detailed, etc)
8. Multi-browser support
9. More JavaDocs

## Discussion
I chose the webapp test because it's a bit more tricky. API's I would have implemented with RestAssured. Let me know if you want those too. Should be a lot faster.

One "sort of bug" is that DOM elements don't always have nice handles. In a real team, I would lobby for the developers to inclide "id=..." and such in to 
elements, for more accurate and easier test authoring

Lots of people like BDD. I am a bit of a Cucumber curmudgeon :) Decided a test like this does not require BDD but can add that, if you'd like.


## To run tests:
0. Prerequisites: Java-19, Maven and Chrome set up and in Path
1. Start node.js server as already explained below

2. Just smoke test: `mvn test -D groups=smoke`
3. All tests: mvn test
4. Nice test reports, after test run: mvn site

## Sort of Test Plan:

1. web01_smoke_test
Basic smoke tests. Just reads the "Dishwashers (..." text in the landing page to make sure we got a live one

2. web02_validate_grid_counter
Compares the count provided at the top of the page with the actual count of items in the grid

3. web03_validate_dishwasher_type_stated
Ensure that we specify in the grid, for each item, if it is Freestanding or Integrated


4. web04_validate_item_link_from_grid
Take some information from the grid about the item (Title and price) and validate that the resulting page has the same info.
This is handy to pick up misdirected links and broken links

5. web05_validate_basic_info_in_item
Validate more in-depth the information in each item page
- Step #1: Image element is rendered and the source leads to some image that isn't completely invalid
- Step #2: Some minimum information for shopping must be available in each item page
- Step #3: Type, as given in the title of the item in the grid, needs to conform with Type, as given in the item detail page
- Step #4: Sometimes, an "Installation Required" field is provided and then this must conform with the type of the washer


## Context

This exercise is part of the recruitment process for Quality Eningeers for the John Lewis Partnership.

Once you have applied for a Quality Enigneering role you are asked to do this exercise as part of the selection process.

This exercise fits into the recruitment process as follows:

1. Complete the application form and supply your CV. (If you are reading this you will have already done that.)
2. Complete the pre-interview exercises which include:
    1. An exercise in critical thinking and continuous testing.
    2. An exercise in displaying knowledge of exploratory testing.
    3. An exercise in writing either
        1. EITHER simple UI automation (<-- the exercise in this repo)
        2. OR simple API test automation.


## Brief

This task asks you to create some simple UI automation tests for the Dishwasher App. This App allows customers to see the range of dishwashers John Lewis sells.

The App has a Product Grid Page and Product Details Pages. When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

Please follow the instructions below to create your own fork of this repository and a local running copy of the App. Then please create some simple automated tests. Store the code for these tests in your fork of the repository.

You may use an automation tool / framework of your choice.

## Expectations

- You will give access to your fork of the repository to a small group of John Lewis Partnership Partners whose names and emil adresses will already have been provided to you.
- Your fork of the repository should have added to it the code you have authored to run your tests.
- You should be able to describe the tests and the risks that they address. Should you progress to the next stage of the recruitment process you may be asked about this.
- We believe you should aim for a maximum of 5 tests. We appreciate that this will involve some investment of your time and so if you only have time to get one working test then that will be fine.


---

## Getting the App running locally

1. Fork this repo into your own GitLab namespace. (You will need a GitLab account.)
1. Clone the project to your local machine. (You will need to be familiar with Git commands executed locally.)
1. Make the directory created by the clone operation the current working directory
1. Install the NPM dependencies using `npm i`. (You will need to have already installed Node.js using version `14.x`.)
1. Run the development server with `npm run dev`
1. You can check that ther server is running by navigating to [http://localhost:3000](http://localhost:3000).
