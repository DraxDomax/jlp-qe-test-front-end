package com.jlc.WebApp.App;

import java.util.ArrayList;
import java.util.List;
import static java.lang.Integer.parseInt;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;



public class GridPageObject extends GenericPageDriver {

    @FindBy(xpath="//*[@id=\"__next\"]/div/main/div/div/h1")
    public WebElement headingAndCounter;

    private int counter;

    @FindBy(xpath="//*[@id=\"__next\"]/div/main/div/div/div")
    private WebElement itemsGrid;

    public GridPageObject(ChromeDriver chromeDriver) {
        super(chromeDriver);
    }

    public int loadGridCounter() {
        String headerFullText = headingAndCounter.getText();
        if (headerFullText.contains("(") && headerFullText.contains(")")) {
            String countAndAfter = headerFullText.split("\\(")[1];
            String countText = countAndAfter.split("\\)")[0];
            counter = parseInt(countText);
            return counter;
        } else {
            throw new AssertionError(
                    "Grid header did not contain an item count or not in expected format '()', got: " +
                            headerFullText);
        }

    }

    public ArrayList<DishwashingMachine> loadGridObjects() {
        ArrayList<WebElement> itemsOnGrid = new ArrayList<>(itemsGrid.findElements(By.xpath("*")));

        ArrayList<DishwashingMachine> result = new ArrayList<>();
        for (WebElement currentItemOnGrid : itemsOnGrid) {
            DishwashingMachine currentDishwashingMachine = new DishwashingMachine();
            currentDishwashingMachine.linkFromGrid = currentItemOnGrid.getAttribute("href");
            currentDishwashingMachine.titleFromGrid = getGridItemTitle(currentItemOnGrid);
            currentDishwashingMachine.priceFromGrid = getGridItemPrice(currentItemOnGrid);
            result.add(currentDishwashingMachine);
        }

        return result;
    }

    public String getGridItemTitle (WebElement gridWebElement) {
        return gridWebElement.findElement(By.xpath("div/div[2]")).getText();
    }
    public String getGridItemPrice (WebElement gridWebElement) {
        return gridWebElement.findElement(By.xpath("div/div[3]")).getText();
    }

}
