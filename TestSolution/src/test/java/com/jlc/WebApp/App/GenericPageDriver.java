package com.jlc.WebApp.App;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Common page-object-model stuff (just one field for now, LOL)
 */
public class GenericPageDriver {

    public ChromeDriver chromeDriver;

    public GenericPageDriver(ChromeDriver chromeDriver) {
        this.chromeDriver = chromeDriver;

        // This line picks up all @FindBy attributes of the page objects and initialises them with actual values on load
        PageFactory.initElements(chromeDriver, this);
    }
}
