package com.jlc.WebApp.App;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class ItemPageObject extends GenericPageDriver {

    @FindBy(xpath="//*[@id=\"__next\"]/div/main/div/h1")
    private WebElement itemTitle;

    @FindBy(xpath="//*[@id=\"__next\"]/div/main/div/div/div[2]/div/div[1]")
    private WebElement itemPrice;

    @FindBy(xpath="//*[@id=\"__next\"]/div/main/div/div/div[1]/div/img")
    private WebElement itemImage;

    @FindBy(xpath="//*[@id=\"__next\"]/div/main/div/div/div[4]/div/ul")
    private WebElement prodSpecList;

    private HashMap<String, String> prodSpecMap;

    public ItemPageObject(ChromeDriver chromeDriver) {
        super(chromeDriver);
    }

    public String getTitle() {
        try {
            return itemTitle.getText();
        } catch (NoSuchElementException noSuchElementException) {
            throw new AssertionError("Item TITLE did not load. Broken link or page structure? Missing Data? ",
                    noSuchElementException);
        }
    }
    public String getPrice() {
        try {
            return itemPrice.getText();
        } catch (NoSuchElementException noSuchElementException) {
            throw new AssertionError("Item PRICE did not load. Broken link or page structure? Missing Data? ",
                    noSuchElementException);
        }
    }

    public void checkImageLoading() {
        String imageSrc = itemImage.getAttribute("src");

        BufferedImage img;
        try {
            // If the URL is not resolvable or the returned stream is empty, this will blow up:
            img = ImageIO.read(new URL(imageSrc));
        } catch (Exception e) {
            throw new AssertionError("Could not load image", e);
        }

        // Just in case an image is somehow returned with an invalid size:
        if ( (img.getWidth()!=0) && (img.getHeight()!=0) ) {
            System.out.printf("Some image loaded with measurements: %s x %s%n",
                    img.getWidth(), img.getHeight());
        } else {
            throw new AssertionError(String.format("Invalid image: %s x %s%n", img.getWidth(), img.getHeight()));
        }
    }

    /**
     * This is the list of product specification in each item's page, for testing more granular information:
     */
    public HashMap<String, String> loadProdSpecMap () {
        prodSpecMap = new HashMap<>();
        ArrayList<WebElement> listItems = new ArrayList<>(prodSpecList.findElements(By.xpath("li")));
        for (WebElement currentListItem : listItems) {
            String key = currentListItem.findElement(By.xpath("div[1]")).getText().toLowerCase();
            String value = currentListItem.findElement(By.xpath("div[2]")).getText().toLowerCase();
            prodSpecMap.put(key, value);
        }
        return prodSpecMap;
    }
}
