package com.jlc.WebApp.App;

/**
 * Local object representation of a dishwashing machine made available on the grid application:
 */
public class DishwashingMachine {
    public String linkFromGrid;
    public String titleFromGrid;
    public String priceFromGrid;
    public boolean integrated;
}
