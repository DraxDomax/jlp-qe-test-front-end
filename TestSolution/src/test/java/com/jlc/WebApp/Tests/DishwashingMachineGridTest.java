package com.jlc.WebApp.Tests;

import com.jlc.WebApp.App.DishwashingMachine;
import com.jlc.WebApp.App.GridPageObject;
import com.jlc.WebApp.App.ItemPageObject;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Stream;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class DishwashingMachineGridTest {

    static ChromeDriver chromeDriver;
    static GridPageObject gridPageObject;
    static ArrayList<DishwashingMachine> dishwashingMachines;

    @BeforeAll
    public static void setUp() {
        // Allows on-the-fly setup of chrome driver, without passing around binary files:
        WebDriverManager.chromedriver().setup();

        chromeDriver = new ChromeDriver();
        chromeDriver.get("http://localhost:3000");

        gridPageObject = new GridPageObject(chromeDriver);
        // Load grid objects into a local representation, for dynamic test case generation:
        dishwashingMachines = gridPageObject.loadGridObjects();
    }

    @Test
    @Tags({@Tag("smoke")})
    public void web01_smoke_test () {
        assert gridPageObject.headingAndCounter.isDisplayed() : "Grid page loaded without 'Dishwashers' title";
    }

    @Test
    public void web02_validate_grid_counter () {
        int countInHeader = gridPageObject.loadGridCounter();
        int countInGrid = dishwashingMachines.size();

        assert countInGrid==countInHeader : String.format("Grid count (%s) does not equal header count (%s)",
                countInGrid, countInHeader);
    }

    @ParameterizedTest
    @MethodSource("com.jlc.WebApp.Tests.DishwashingMachineGridTest#dishWashTestCaseStreamer")
    public void web03_validate_dishwasher_type_stated (DishwashingMachine currentDishwasher) {
        System.out.println("Testing: " + currentDishwasher.titleFromGrid);

        if (currentDishwasher.titleFromGrid.toLowerCase().contains("integrated")) {
            currentDishwasher.integrated = true;
        } else if (currentDishwasher.titleFromGrid.toLowerCase().contains("freestanding")) {
            currentDishwasher.integrated = false;
        } else {
            throw new AssertionError("Title of item in grid did not specify integrated/freestanding. " +
                    "Title: " + currentDishwasher.titleFromGrid);
        }
    }

    @ParameterizedTest
    @MethodSource("com.jlc.WebApp.Tests.DishwashingMachineGridTest#dishWashTestCaseStreamer")
    public void web04_validate_item_link_from_grid (DishwashingMachine currentDishwasher) {
        System.out.println("Testing: " + currentDishwasher.titleFromGrid);

        chromeDriver.get(currentDishwasher.linkFromGrid);
        ItemPageObject itemPage = new ItemPageObject(chromeDriver);

        // Validate Title is the same from grid to item page:
        assert itemPage.getTitle().equalsIgnoreCase(currentDishwasher.titleFromGrid) : "Item TITLE did not match " +
                "title from grid. \n" +
                "Grid: " + currentDishwasher.titleFromGrid + "\nInside: " + itemPage.getTitle();

        // Validate Price is the same from grid to item page:
        assert itemPage.getPrice().equals(currentDishwasher.priceFromGrid) : "Item PRICE did not match " +
                "PRICE from grid. \n" +
                "Grid: " + currentDishwasher.priceFromGrid + "\nInside: " + itemPage.getPrice();
    }

    @ParameterizedTest
    @MethodSource("com.jlc.WebApp.Tests.DishwashingMachineGridTest#dishWashTestCaseStreamer")
    public void web05_validate_basic_info_in_item (DishwashingMachine currentDishwasher) {
        System.out.println("Testing: " + currentDishwasher.titleFromGrid);

        chromeDriver.get(currentDishwasher.linkFromGrid);
        ItemPageObject itemPage = new ItemPageObject(chromeDriver);

        // Validate top-level sections of product information
        // Image isn't a broken source link:
        itemPage.checkImageLoading();
        // Information and Specification sections:
        assert chromeDriver.findElement(By.xpath(
                "//h3[normalize-space()='Product information']")).isDisplayed();
        assert chromeDriver.findElement(By.xpath(
                "//h3[normalize-space()='Product specification']")).isDisplayed();

        // Validate information for minimum fields which are absolutely mandatory is provided:
        HashMap<String, String> loadedMap = itemPage.loadProdSpecMap();
        String[] mandatoryFields = {"type", "noise level rating", "dimensions", "brand", "energy rating - overall"};
        for (String mandatoryField : mandatoryFields) {
            String specValue = loadedMap.get(mandatoryField);
            if (specValue == null || specValue.isEmpty()) {
                throw new AssertionError("Specification missing or empty value: " + mandatoryField);
            }
        }

        // Compare type from grid title to type in product specification:
        String specIntegrated = loadedMap.get("type");
        if (currentDishwasher.integrated) {
            assert specIntegrated.equalsIgnoreCase("integrated"):
                    "Type is integrated but 'Integrated' specification' is: " + specIntegrated;
        } else {
            assert specIntegrated.equalsIgnoreCase("freestanding"):
                    "Type is freestanding but 'Integrated' specification' is: " + specIntegrated;
        }

        // Compare type from grid title to whether the information indicates required installation or not:
        String specInstallationReq = loadedMap.get("installation required");
        if (specInstallationReq != null) {
            if (currentDishwasher.integrated) {
                assert specInstallationReq.equalsIgnoreCase("yes") :
                        "Type is integrated but 'Installation Required' is: " + specInstallationReq;
            } else {
                assert specInstallationReq.equalsIgnoreCase("no"):
                        "Type is freestanding but 'Installation Required' is: " + specInstallationReq;
            }
        }
    }

    @AfterAll
    public static void tearDown() {
        chromeDriver.close();
    }

    /**
     * Used in instantiating JUnit objects (unit tests) for each grid item:
     */
    private static Stream<DishwashingMachine> dishWashTestCaseStreamer() {
        return dishwashingMachines.stream();
    }


}